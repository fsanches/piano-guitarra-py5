"""
(c)2024 Felipe Corrêa da Silva Sanches <juca@members.fsf.org>
This code is licensed under the terms of the GPL version 3 or later.

Drawing diagrams similar to the ones seen at:
https://www.libertyparkmusic.com/read-guitar-chord-diagrams/
"""
import py5
import mido
import math

ioport = None
notes = None

open_string_notes = [
    3*12 + 4,  # E3
    3*12 + 9,  # A3
    4*12 + 2,  # D4
    4*12 + 7,  # G4
    4*12 + 11, # B4
    5*12 + 4   # E5
]

num_frets = 6  # TODO: how many frets does a guitar usually have?


def numbers_to_guitar(notes):
    notes = sorted(notes)
    guitar = [-1] * 6
    for note in notes:
        for i, guitar_string in enumerate(guitar):
            if guitar_string != -1:
                continue
            if note in range(open_string_notes[i], open_string_notes[i] + num_frets):
                guitar[i] = note - open_string_notes[i]
                break
    return guitar


screen_width = 1024
screen_height = 1024
fret_h_unit = math.floor((screen_height * 0.7) / 5)
fret_w_unit = fret_h_unit * 0.6


def setup():
    global ioport
    global notes
    py5.size(screen_width, screen_height)
    try:
        port_name = mido.get_ioport_names()[-1]
        print(f"Using port '{port_name}'")
        ioport = mido.open_ioport(port_name)
    except:
        pass
    notes = set()


def draw_fret_diagram(guitar):
    py5.fill(255)
    py5.stroke_weight(4)
    left_x = math.floor(screen_width/2 - 2.5 * fret_w_unit)
    top_y = math.floor(screen_height/2 - 2.5 * fret_h_unit)

    py5.push_matrix()
    py5.reset_matrix()

    thickness = fret_h_unit*0.1
    py5.stroke(0)
    py5.fill(0)
    py5.rect(left_x,          top_y - thickness,
             5 * fret_w_unit, thickness)

    py5.fill(255)
    py5.translate(left_x, top_y)
    for i in range(5):
        for j in range(5):
            py5.rect(i * fret_w_unit, j * fret_h_unit,
                     fret_w_unit,     fret_h_unit)

    radius = fret_w_unit * 0.7
    for i in range(6):
        if guitar[i] == -1:
            # draw X
            py5.line(i * fret_w_unit - radius/2.5, -0.5 * fret_h_unit - radius/2.5,
                     i * fret_w_unit + radius/2.5, -0.5 * fret_h_unit + radius/2.5)
            py5.line(i * fret_w_unit + radius/2.5, -0.5 * fret_h_unit - radius/2.5,
                     i * fret_w_unit - radius/2.5, -0.5 * fret_h_unit + radius/2.5)
            pass
        elif guitar[i] == 0:
            # draw hollow-circle
            py5.fill(255)
            py5.circle(i * fret_w_unit, -0.5 * fret_h_unit, radius)

            py5.fill(0)
            note = open_string_notes[i]
            py5.text_size(36)
            py5.text_align(py5.CENTER, py5.CENTER)
            py5.text(number_to_note(note, octave=False),
                     i * fret_w_unit,
                     (-0.5) * fret_h_unit)
        else:
            # draw filled-circle
            py5.fill(0)
            py5.circle(i * fret_w_unit, (0.5 + guitar[i] - 1) * fret_h_unit, radius)

            py5.fill(255)
            note = open_string_notes[i] + guitar[i]
            py5.text_size(36)
            py5.text_align(py5.CENTER, py5.CENTER)
            py5.text(number_to_note(note, octave=False),
                     i * fret_w_unit,
                     (0.5 + guitar[i] - 1) * fret_h_unit)
    py5.pop_matrix()


num_octaves = 8
white_key_width = 16
black_key_width = 12
keybed_width = 7 * white_key_width * num_octaves
keybed_height = white_key_width * 4
black_key_height = white_key_width * 2.5

def draw_piano_diagram(notes):
    py5.push_matrix()
    py5.reset_matrix()


    left_x = math.floor(screen_width/2 - 0.5 * keybed_width)
    top_y = math.floor(screen_height - keybed_height - 50)
    py5.translate(left_x, top_y)

    py5.stroke_weight(1)

    # draw white-keys:
    for i in range(7*num_octaves):
        octave = math.floor(i/7)
        note = 12*octave + [0, 2, 4, 5, 7, 9, 11][i%7]
        py5.stroke(128)
        if note in notes:
            py5.fill(py5.color(0, 255, 255))
        else:
            py5.fill(255)

        py5.rect(i * white_key_width, 0,
                 white_key_width, keybed_height)

    # draw black-keys:
    py5.stroke(128)
    py5.fill(220)
    for i in range(5*num_octaves):
        octave = math.floor(i/5)
        note = 12*octave + [1, 3, 6, 8, 10][i%5]
        if note in notes:
            py5.fill(py5.color(0, 255, 255))
        else:
            py5.fill(220)

        x_offset = [1, 3, 6, 8, 10][i%5] / 12
        py5.rect(7 * white_key_width * (octave + x_offset), 0,
                 7 * white_key_width / 12, black_key_height)

    py5.pop_matrix()


def print_text(text_string, line_spacing=40):
    py5.text(text_string, 0, 0)
    py5.translate(0, line_spacing)


def number_to_note(note, octave=True):
    note_name = ["C", "Db", "D", "Eb", "E", "F", "F#", "G", "Ab", "A", "Bb", "B"][note % 12]
    if octave:
        octave_number = math.floor(note/12)
        return f"{note_name}{octave_number}"
    else:
        return note_name


def draw():
    global ioport, notes
    py5.background(255)
    py5.text_align(py5.CENTER, py5.CENTER)
    py5.text_size(24)
    py5.translate(screen_width/2, screen_height - 30)

    if ioport:
        msg = ioport.receive()
        if msg.type == "note_on" and msg.velocity > 0:
                notes.add(msg.note)
        elif msg.type == "note_off" or \
            (msg.type == "note_on" and msg.velocity == 0):
            if msg.note in notes:
                notes.remove(msg.note)
    else:
        notes = set([40, 47, 53, 61])

    py5.fill(0)
    guitar = numbers_to_guitar(notes)
    print_text(" - ".join(list(map(number_to_note, sorted(notes)))))
    draw_fret_diagram(guitar)
    draw_piano_diagram(notes)


def exiting():
    if ioport:
        ioport.close()


py5.run_sketch()
